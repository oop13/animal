/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.inheritance;

/**
 *
 * @author Sarocha
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Chaser","White",0);
        animal.speak();
        animal.walk();
        space();
        Dog dang = new Dog("Dang","Black&White");
        dang.speak();
        dang.walk();
        space();
        Cat zero = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        space();
        Duck zom = new Duck("Zom","Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        space();
        Dog to = new Dog("To","Brown");
        to.speak();
        to.walk();
        space();
        Dog mome = new Dog("Mome","Black&White");
        mome.speak();
        mome.walk();
        space();
        Dog bat = new Dog("Bat","Black&White");
        bat.speak();
        bat.walk();
        space();
        Duck gabgab = new Duck("GabGab","Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        space();
        System.out.println("Zom is Animal : " + (zom instanceof Animal));
        System.out.println("Zom is Duck : " + (zom instanceof Duck));
        System.out.println("Zom is Cat : " + (zom instanceof Object));
        System.out.println("Animal is Dog : " + (animal instanceof Dog));
        System.out.println("Animal is Animal : " + (animal instanceof Animal));
        System.out.println("To is Animal : " + (to instanceof Animal));
        System.out.println("To is Dog : " + (to instanceof Dog));
        System.out.println("Zero is Animal : " + (zero instanceof Animal));
        System.out.println("Zero is Cat : " + (zero instanceof Cat));
        space();
        space();
        Animal[] animals = {dang, zero, zom, to, mome, bat, gabgab};
        for (int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck) {
               Duck duck = (Duck)animals[i];
               duck.fly();
            }
            space();
        }
    }
    
    public static void space() {
        System.out.println();
    }
}
